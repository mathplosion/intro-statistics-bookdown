This is the textbook for a introductory statistics class written using bookdown and mathplosion.

The built version of the textbook can be seen here:

https://mathplosion.gitlab.io/intro-statistics-bookdown

To build it you need to install R, pandoc, pandoc-citeproc, a latex installation and then the bookdown package

Once R is installed you should install the following in a shell:

```
sudo apt-get install -y --no-install-recommends pandoc pandoc-citeproc
wget -qO- "https://yihui.name/gh/tinytex/tools/install-unx.sh" | sh
R -q -e 'install.packages(c("bookdown"), repos = "https://cran.rstudio.com/")'
```

Now to build the gitbook you can do this:

```
make
```
To build the pdf you can do this

```
make pdf
```

See the Makefile for more information.

The problems directory holds the Mathplosion problems

The _book directory holds the builds. 



