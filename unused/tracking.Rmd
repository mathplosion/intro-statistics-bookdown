```{r setup, echo=FALSE}
source("timeseries.R")
library("statplosion")

plot_time_series <- function(data1, data2, main="", xlims=c(1,12), ylims=c(0,100)) {
    ts1 <- ts(data1, start=1, freq=1)
    ts2 <- ts(data2, start=1, freq=1)
    series<-cbind(ts1, ts2)
    names <- c("Demand", "Forecast") 
    plot_periods(series, 
                 type="o", 
                 labels = c("Months", "Units"),
                 xlims=xlims, 
                 ylims=ylims, 
                 names=names,
                 main=main) 
}
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

# Tracking

Tracking is an idea that captures the bias of a forecast compared to
actual demand history. 

- If a forecast is repeatedly hitting on the high side of actual demand then we will say that is **positive track**.
- If a forecast is repeatedly hitting to the low side of the actual demand then we will say that is **negative track**.
 
For us track will be a number between -1.0 (largest negative track) and
+1.0 (largest positive track).

Some systems use -100% to +100% instead of -1.0 to 1.0. We stick with the decimal version. 

If track is close to 0, then that means that typically we are hitting close to the actual demand, maybe a little above sometimes, maybe a little below sometimes. But this is a good sign for track to be close to 0. 

## Tracking As a Sign of a Problem

If tracking drifts off away from 0 and towards -1.0 or +1.0 and stays that way over time it is a sign that the demand and forecast are drifting apart.

At that point we have to ask why the forecast is repeatedly erroring to one side or the other of our actual demand. Is there some reason that our forecast is repeatedly either too high or too low?  

Possibly it is time to use a different forecasting method. (Remember we could be using naive or moving average or weighted average or exponential smoothing or many other forecasting methods.)

## Positive Track Example

Here the forecast repeatedly hits above the actual demand, except for a couple of exceptions:

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
forecast <- c(56,52,68,44,72,85,82,63,70,85,62,72)
demand <-   c(50,55,65,38,66,70,75,52,75,80,45,70)
plot_time_series(demand, forecast)
```

## Negative Track Example

Here the forecast repeatedly hits below the actual demand, except for one exception:

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
forecast <- c(40,50,61,32,70,62,68,48,63,74,40,60)
demand <-   c(50,55,65,38,66,70,75,52,75,80,45,70)
plot_time_series(demand, forecast)
```

## Using Track to Adjust Forecasts

It turns out that tracking can be used to adjust forecasts to be more in
line with demand. 

The way it works is that we have an "original" forecast that we have made using some method (and maybe some months ago). But at each month we look at that most recent demand and re-evaluate the forecast for the future:

At each period, form a "weighted average" of the last Demand and
the last Forecast. Use the size of the track (ignoring the sign) as the
weight for the Demand:

$$ 
\text{New Forecast} = (track)(\text{Demand}) + (1-track)(\text{Orig Forecast}) 
$$

Why does this make sense?

- If the track is close to 0 then this will put most of the weight on the
last Forecast.  
- If the track is close to 1.0, then this will mean most weight is on the last Demand.

Lets think about why this makes sense if we need to "adjust" our forecast.


**Example: (track close to 0)**:

Say track = +0.1 

$$ 
\text{New Forecast} = (.1)(\text{Demand}) + (.9)(\text{Orig Forecast}) 
$$

Since the track is close to 0 above this gives most weight to the original Forecast. That makes sense because track close to 0 means things are okay, and the original Forecast is fine. So not much adjustment needs to be done.

**Example: (track close to 1)**:

Say track = +0.9 

$$ 
\text{New Forecast} = (.9)(\text{Demand}) + (.1)(\text{Orig Forecast}) 
$$

Since the track is close to 1.0 this gives most weight to the most recent Demand. That makes sense because track close to +1.0 means the forecast and demand are drifting apart, and the forecast should be moved to be more in line with the actual demand. The original forecast is not that reliable, so we give smaller weight to it in the weighted average above.

## Why We Use Track to Adjust Forecasts

What would happen if we did not do track adjusted forecasting?

If we did no track adjustment, the Forecast would not be as responsive
to sudden changes in Demand. Here is an example where there is a jump up
in Demand and it takes a while for the Forecast to come up and follow
the Demand.

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
demand<-c(50,58,54,67,65,55,89,50,68,160,170,180,150,130)
forecast<-c(50.00,50.00,51.60,52.08,55.06,57.05,56.64,63.11,60.49,61.99,81.59,99.27,115.42,122.34);
plot_time_series(demand, forecast, main="No Track Adjustment", 
                 xlims=c(1,14), ylims=c(0,200))
```

Here the Forecast reacts only slowly over 4 or 5 months to the big jump in Demand in month 10. 

However if we use the track as described above, we see something more
like the following:

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
demand<-c(50,58,54,67,65,55,89,50,68,160,170,180,150,130)
forecast<-c(50.00,50.00,58.00,54.89,65.11,65.02,61.71,79.58,79.55,77.67,120.83,153.77,172.81,162.38)
plot_time_series(demand, forecast, main="With Track Adjustment", 
                 xlims=c(1,14), ylims=c(0,200))
```

Notice how the forecast is responsive and follows the Demand better as
it tracks away from the Forecast in period 10.

So using track in the weighted average of Demand and Forecast makes the
system more responsive to sudden jumps or "shocks" in the Demand (which
show up as bias in the tracking signal). 

Reforecasting using the most recent demand brings the Forecast more quickly in line with the Demand.
