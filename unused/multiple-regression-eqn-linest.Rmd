```{r setup, echo=FALSE}
#library("kableExtra")
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
	border: 1px solid #666;
	padding: 5px; 
	min-width: 70px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

## Using LINEST for Multiple Regression Coefficients 

## The Data  

A small cosmetics company Mysteria believes sales (in thousands of dollars) at its stores is affected by advertising expenditures and by the population size of the vicinity of the area. 

They collect data for 6 stores and get the following. 

|     |     |       |
|-----|-----|-------|
| Adv (x1) | Pop (x2) | Sales (y) |
| 50  | 5   | 350   |
| 55  | 4   | 400   |
| 100 | 8   | 500   |
| 30  | 1.5 | 300   |
| 25  | 3.5 | 350   |
| 60  | 1   | 400   |

where:

- Adv ($x_1$) is Advertising Expenditures in ($ thousands) for the store
- Pop ($x_2$) is Population Size in (millions) for the vicinity
- Sales ($y$) is Sales in ($thousands) for the store

Make sure this data is entered into a spreadsheet in columns A and B and C of the spreadsheet.

## LINEST output 

Enter the following in cell F3 

**=LINEST(C2:C7,A2:B7,TRUE,TRUE)**

(notice how y range is first, then the x-ranges are together in two columns)


|    | A   | B   | C     | D | E | F            | G            | H           | I |
|----|-----|-----|-------|---|---|--------------|--------------|-------------|---|
| 1  | Adv | Pop | Sales |   |   |              |              |             |   |
| 2  | 50  | 5   | 350   |   |   |              |              |             |   |
| 3  | 55  | 4   | 400   |   |   | 2.583255524  | 2.225334578  | 254.746343  |   |
| 4  | 100 | 8   | 500   |   |   | 7.263646048  | 0.6899796916 | 29.28252658 |   |
| 5  | 30  | 1.5 | 300   |   |   | 0.8859543818 | 29.78290576  | #N/A        |   |
| 6  | 25  | 3.5 | 350   |   |   | 11.65263158  | 3            | #N/A        |   |
| 7  | 60  | 1   | 400   |   |   | 20672.26891  | 2661.064426  | #N/A        |   |
| 8  |     |     |       |   |   |              |              |             |   |
| 9  |     |     |       |   |   |              |              |             |   |

## Interpreting the LINEST output 

The LINEST output is in F3:H7, the last three #N/A in column H are normal... there is no output for those cells.

Remember that:

- Adv ($x_1$) is the first column of x-variables 
- Pop ($x_2$) is the second column of x-variables

But:

**WARNING: The coefficients (slopes) for the LINEST output are in \"backwards\" order**:

- Pop ($x_2$) slope $m_2$ is first
- Adv ($x_1$) slope $m_1$ is second 

So we have these for the coefficients:

- **Pop** coefficient is (2.58...) in F3
    - This is the slope for the $Pop$ variable.
- **Adv** coefficient (2.22...) in G3
    - This is the slope for the $Adv$ variable.
- **intercept b** (254.74) in H3
    - This is the intercept

Usually **you just can just list the x-variables backwards above the LINEST output**.

So in F2,G2,and H2 like this: 

|    | A   | B   | C     | D | E | F            | G            | H           | I |
|----|-----|-----|-------|---|---|--------------|--------------|-------------|---|
| 1  | Adv | Pop | Sales |   |   |              |              |             |   |
| 2  | 50  | 5   | 350   |   |   | **Pop**      | **Adv**      | **b**       |   |
| 3  | 55  | 4   | 400   |   |   | 2.583255524  | 2.225334578  | 254.746343  |   |
| 4  | 100 | 8   | 500   |   |   | 7.263646048  | 0.6899796916 | 29.28252658 |   |
| 5  | 30  | 1.5 | 300   |   |   | 0.8859543818 | 29.78290576  | #N/A        |   |
| 6  | 25  | 3.5 | 350   |   |   | 11.65263158  | 3            | #N/A        |   |
| 7  | 60  | 1   | 400   |   |   | 20672.26891  | 2661.064426  | #N/A        |   |
| 8  |     |     |       |   |   |              |              |             |   |
| 9  |     |     |       |   |   |              |              |             |   |

So the multiple regression equation will be this:

$$
Sales = (2.58)Pop + (2.23)Adv + 254.75 
$$

If you wanted to do a prediction for the Sales when the Population size was 6 million and the Advertising Expenditures were $40,000, you would do this like this: 

$$
Sales = (2.58)Pop + (2.23)Adv + 254.75 = (2.58)(6) + (2.23)(40) + 254.75 = `r  (2.58)*(6) + (2.23)*(40) + 254.75`
$$

