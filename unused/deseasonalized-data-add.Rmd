---
pagetitle: "Deseasonalized Data (Residual)"
---

```{r setup, echo=FALSE}
#source("spreadsheet.R")
source("timeseries.R")
library("statplosion")

spreadsheet_seasonal<- function(demand, baseline, indices) {
	periods <- 1:13
	columns <- list(periods, demand, baseline, indices)
	names(columns) <- c("Month", "Demand", "Baseline", "Index")
    create_spreadsheet_with_data(columns, size=c(14,10))
}

plot_demand <- function(demand) {
    demand_ts <- ts(demand, start=c(2015,1), frequency=12)
    series <- cbind(demand_ts)
    names <- c("Demand") 
    plot_monthly_months(series, 
                 type="l", 
                 labels = c("Months", "Units"),
                 format = "%b",
                 main="Demand",
                 names=names) 
}

plot_residual <- function(residual) {
    residual_ts <- ts(residual, start=c(2015,1), frequency=12)
    series <- cbind(residual_ts)
    names <- c("Residual") 
    plot_monthly_months(series, 
                 type="l", 
                 labels = c("Months", "Units"),
                 format = "%b",
                 colors = c("brown"),
                 main="Residual",
                 names=names) 

    abline(h=0)
}

plot_demand_and_composite <- function(demand, composite) {
    demand_ts <- ts(demand, start=c(2015,1), frequency=12)
    composite_ts <- ts(composite, start=c(2015,1), frequency=12)
    series <- cbind(demand_ts, composite_ts)

    names <- c("Demand", "Seasonal Component") 

    plot_monthly_months(series, 
                 type="l", 
                 labels = c("Months", "Units"),
                 colors = c("red","blue"),
                 format = "%b",
                 main="Demand and Seasonal Component (Composite)",
                 names=names) 
}

plot_demand_deseasonal_regression_forecast <- function(deseasonal, regression,  forecast) {
    deseasonal_ts <- ts(deseasonal, start=c(2015,1), frequency=12)
    regression_ts <- ts(regression, start=c(2016,1), frequency=12)
    forecast_ts <- ts(forecast, start=c(2016,1), frequency=12)
    series <- cbind(deseasonal_ts, regression_ts, forecast_ts)

    names <- c("Deseasonalized Demand", 
               "Regression Line", 
               "Forecast (reseasonalized)") 

    plot_monthly_months(series, 
                 type="o", 
                 ylims = c(5,160), 
                 labels = c("Months", "Units"),
                 colors = c("blue","green", "brown"),
                 format = "%b",
                 main="Deseasonalized Demand and Regression Line",
                 names=names) 
}
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

# Deseasonalizing Data (Using Residuals)

Another way to **deseasonalize data** that is convenient sometimes is to
**remove the seasonal component by subtraction**. In this situation we do
not use the seasonal indices but instead just subtract a component of the data that represents the average seasonal data. We just take the average demand in each period, so this represents the typical ups and downs due to seasonality.

After we subtract out this component, this leaves us with deseasonalized data (called the **residual** here) that does not contain the seasonal pattern. For some periods the residual is positive and for other periods the residual is negative, and these positive and negative values have an important interpretation we discuss below.

## Three Years of Demand 

Let's start with some historical data ($Y$) that has seasonality.

```{r}
year1 <- c(2,3,7,8,28,20,25,7,15,6,2,12)
year2 <- c(6,7,3,5,32,25,32,5,7,3,5,12)
year3 <- c(1,3,3,2,34,18,27,5,9,3,2,14)
demand<- c(year1,year2,year3)
df <- data.frame(year1 = year1, year2=year2, year3=year3)
df_transpose <- as.data.frame(t(as.matrix(df)))
names(df_transpose)<-1:12
kable(df_transpose, "markdown", row.names = TRUE)
```

Here is a graph of the data

```{r}
plot_demand(demand)
```

## Seasonal Component (or Composite)

First we find the composite or average for each period of all the historical demand. This involves an average of each month's demands for all three years that we have. This looks something like this:

- Period 1 $$\frac{2+6+1}{3} = 3.0$$
- Period 2 $$\frac{3+7+3}{3} = 4.33$$
- Period 3 $$\frac{7+3+3}{3} = 4.33$$

And so on ...   
    
This gives us the following **seasonal component** (or composite). We will call it $S$:

```{r}
composite<- (year1+year2+year3)/3
composite_df <- data.frame(composite = round(composite,2))
composite_df_transpose <- as.data.frame(t(as.matrix(composite_df)))
names(composite_df_transpose)=1:12
kable(composite_df_transpose, "markdown", row.names = TRUE)
```

If we had to describe this we would say:

- In month 1, the average demand is `r round(composite[1],2)`
- In month 2, the average demand is `r round(composite[2],2)`
- In month 3, the average demand is `r round(composite[3],2)`

and so on.  
    
## Deseasonalize by Subtracting

Now to ***deseasonalize*** (using this additive model) we just subtract the composite from the original demand for each of the 3 years of historical data that we have.  We call this the residual ($R$) and it represents our deseasonalized data.

$$ 
\text{residual}  = \text{demand}-(\text{seasonal component}) 
$$

$$
R = Y - S
$$


#### Year 1:

- Period 1 $$Y - S = 2-3=-1.00$$
- Period 2 $$Y - S = 3-4.33=-1.33$$

#### Year 2:

- Period 1 $$Y - S = 6-3=3.00$$
- Period 2 $$Y - S = 7-4.33=2.67$$

#### Year 3:

- Period 1 $$Y - S = 6-3=3.00$$
- Period 2 $$Y - S = 7-4.33=2.67$$

And so on for all three years. This gives us this:

## Residual

Here is the original demand data($Y$) compared to composite seasonal
component ($S$):

```{r}
full_composite<- c(composite, composite, composite) 
plot_demand_and_composite(demand, full_composite)
```

For this graph, keep in mind that the **red series (demand)** is **how we did**, and the **blue series (composite)** is **how we do on average**. So 

- if we see the demand(red) above the composite(blue) we did better than usual 
- if we see the demand(red) below the composite(blue) we did worse than usual 

For example during the first year in months April-July we did worse than usual (since composite was above demand those months), then in the second year in months April-July we did better than usual (since demand is abobve the composite those months).

If we subtract these two graphs, we can look at the residual on its own:

```{r}
full_composite<- c(composite, composite, composite) 
residual <- demand - full_composite
plot_residual(residual)
```

This graph now makes it easy to detect which months were "better than average" and which months were "worse than average":

- If the **residual is positive**, that means we had a **better than average** month. 
- If the **residual is negative**, that means we had a **worse than average** month.

Now that seasonality has been removed, it may be useful to analyze the
residual series further with methods like regression. For example overall  trend components may be more easily identified if the seasonality is removed.
