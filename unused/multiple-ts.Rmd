```{r setup, echo=FALSE}
source("timeseries.R")
library("statplosion")

spreadsheet_multiple<- function(demand, forecast, whatever) {
	months <- 1:8
	columns <- list(months, demand, forecast, whatever)
	names(columns) <- c("Months", "Demand", "Forcecast", "Whatever")

        create_spreadsheet_with_data(columns)
}


plot_multi_time_series <- function(data1, data2, data3, data4) {

    ts1 <- ts(data1, start=1, freq=1)
    ts2 <- ts(data2, start=2, freq=1)
    ts3 <- ts(data3, start=5, freq=1)
    ts4 <- ts(data4, start=3, freq=1)

    series<-cbind(ts1, ts2, ts3, ts4)

    names <- c("First", "Second", "Third", "Fourth") 

    plot_periods(series, 
                 type="o", 
                 labels = c("Months", "Units"),
                 xlims=c(1,12), 
                 ylims=c(0,100), 
                 names=names) 
}
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

```{r, eval=F}
```

# Multiple Time Series

## A Simple Technique For Forecasting

## Demand for Month 1 

Here is the graph: 

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
demand <- c(50,55)
deseasoned <- c(60,65,56)
forecast <- c(70,33,54)
whatever <- c(30,44,60,50)
plot_multi_time_series(demand, deseasoned, forecast, whatever)
```

Now lets see a spreadsheet with this data on it.

```{r, fig.width=6, fig.height=5, fig.align="center", eval=T}
spreadsheet_multiple(demand, forecast, whatever)
```
