```{r setup, echo=FALSE}
library("statplosion")
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

# Regression Equation using SLOPE and INTERCEPT

## Data

Here is the data set we will work with:   
        
    
```{r}
year<- 1:4
sales <- c(53,56,58,58)
data_df<-data.frame(year=1:4, 
	       sales=c(53,56,58,58), 
	       stringsAsFactors=FALSE) 

colnames <- colnames(data_df)
kable(data_df, "markdown")
```

The goal here will be to predict $`r colnames[2]`$ from $`r colnames[1]`$.

In order to do that we have to come up with an equation that will allow us to plug in an x-value ($`r colnames[1]`$) and get out a y-value ($`r colnames[2]`$). This is what we mean by prediction.  

First lets see a scatterplot of the data:

```{r fig.height=4, fig.width=5, fig.align="center"}
plot(data_df, pch=19)
```

So start by copying the data into your spreadsheet in columns A and B:   
    
```{r}
df <- create_blank_spreadsheet(10, 6)

df <- insert_data(df, data_df)
kable(df, "markdown", row.names=TRUE)
```

## Finding Slope and Intercept of Regression Line 

Next lets compute the slope and intercept using the functions SLOPE and INTERCEPT in a spreadsheet. Make sure you enter the y range in first and then the x range:

```{r}
colnames <- colnames(data_df)
str <- paste(colnames[2],colnames[1],sep="~")
model <- lm(str, data=data_df)

df[10,"E"] <- strrep("&nbsp; ", 23) 
df[10,"D"] <- strrep("&nbsp; ", 11) 

df<-regress_insert_m_and_b(df, 2, 4, model, formula=TRUE)
kable(df, "markdown", row.names=TRUE)
```

Here are the values for the slope and intercept:

```{r}
df<-regress_insert_m_and_b(df, 2, 4, model, formula=FALSE)
kable(df, "markdown", row.names=TRUE)
```

From this we see that the regression equation is given below:  
     
```{r}
m <- unname(model$coefficients[2])
b <- unname(model$coefficients[1])
```
$$ 
`r colnames[2]`= `r m`(`r colnames[1]`) + `r b`
$$


## Making Predictions Using the Regression Equation 

Next we will make some predictions using the equation above. We want to make predictions for the following values:    

```{r}
predicters <- c(5,6)
predicters_df<-data.frame(year=predicters, 
	       sales=c("?","?"), 
	       stringsAsFactors=FALSE) 
kable(predicters_df, "markdown")
```

So first we set up a part of the spreadsheet to hold our predictions. We call this sections "predictions". Then we list the x-values that will make predictions for. Then along side that we fill in a formula for the regression equation. We use the entries for the slope and the intercept that we found above:

```{r}
df<-regress_insert_predictions(df, predicters, 5, 4, model, formula=TRUE)
kable(df, "markdown", row.names=TRUE)
```

Finally here are the values of the predictions we just entered above:

```{r}
df<-regress_insert_predictions(df, predicters, 5, 4, model, formula=FALSE)
kable(df, "markdown", row.names=TRUE)
```

## A Plot of the Predictions 

Below we show the regression line

$$ 
`r colnames[2]`= `r m`(`r colnames[1]`) + `r b`
$$

and the predictions for $year=5$ and $year=6$

```{r fig.height=4, fig.width=5, fig.align="center"}
plot(data_df, xlim=c(1,6), ylim=c(53,65), pch=19)
points(x=c(5,6),y=c(60.5,62.2), col="blue", pch=19)
abline(model)
```
